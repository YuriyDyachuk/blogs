<?php

namespace App\Providers;

use App\Models\Comment;
use App\Models\Post;
use App\Models\Category;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('pages._sidebar', function ($view){
           $view->with('popularPosts', Post::getPopularPosts());
           $view->with('featuredPosts', Post::getFeaturedPosts());
           $view->with('recentPosts', Post::getRecentPosts());
           $view->with('categories', Category::all());
        });

        view()->composer('admin._sidebar', function ($view){
            $view->with('newCommentsCount', Comment::getNewCommentsCount());
        });
    }
}
