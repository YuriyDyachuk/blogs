<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Post;
use App\Models\Tag;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        $posts = Post::paginate(3);
        return view('pages.index', [
            'posts' => $posts,
        ]);
    }

    public function show($slug)
    {
        $post = Post::where('slug' , $slug)->firstOrFail();
        return view('pages.show',[ 'post' => $post]);
    }

    public function tag($slug)
    {
        $tag = Tag::where('slug', $slug)->firstOrFail();
        $posts = $tag->posts()->paginate(3);

        return view('pages.list',['posts' => $posts]);
    }

    public function category($slug)
    {
        $cat = Category::where('slug', $slug)->firstOrFail();
        $posts = $cat->posts()->paginate(3);

        return view('pages.list',['posts' => $posts]);
    }
}
