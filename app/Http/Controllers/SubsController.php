<?php

namespace App\Http\Controllers;

use App\Mail\SubscribeEmail;
use App\Models\Subscription;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class SubsController extends Controller
{
    public function subscribe(Request $request)
    {
        $this->validate($request, [
            'email'	=>	'required|email|unique:subscriptions'
        ]);

        $subs = Subscription::add($request->get('email'));
        $subs->generateToken();

        Mail::to($request->get('email'))->send(new SubscribeEmail($subs));
        return redirect()->back()->with('success','Проверьте вашу почту!');
    }

    public function verify($token)
    {
        $subs = Subscription::where('token',$token)->firstOrFail();
        $subs->token = null;
        $subs->save();

        return redirect('/')->with('success','Ваша почта подтверждена.СПАСИБО :)');
    }
}
