<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Subscription;
use Illuminate\Http\Request;

class SubscribersController extends Controller
{

    public function index()
    {
        $subs = Subscription::all();
        return view('admin.subs.index',['subs' => $subs]);
    }


    public function create()
    {
        return view('admin.subs.create');
    }


    public function store(Request $request)
    {
        $this->validate($request,[
            'email' => 'required|email|unique:subscriptions',
        ]);

        Subscription::add($request->get('email'));
        return redirect()->route('subscribers.index')->with('success','Подписчик удачно создан :)');
    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        //
    }


    public function update(Request $request, $id)
    {
        //
    }


    public function destroy($id)
    {
        Subscription::find($id)->remove();
        return redirect()->route('subscribers.index')->with('success','Подписчик удалён.)');
    }
}
