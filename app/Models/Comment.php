<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    const IS_STATUS_DISABLED = 0;
    const IS_STATUS_PUBLISH = 1;

    public function author()
    {
        return $this->belongsTo(User::class,'user_id');
    }

    public function posts()
    {
        return $this->belongsTo(Post::class);
    }

    public function allow()
    {
        $this->status = Comment::IS_STATUS_PUBLISH;
        $this->save();
    }

    public function disabled()
    {
        $this->status = Comment::IS_STATUS_DISABLED;
        $this->save();
    }

    public function toggleStatus()
    {
        if ($this->status == Comment::IS_STATUS_DISABLED)
        {
            return $this->allow();
        }

        return $this->disabled();
    }

    public function remove()
    {
        $this->delete();
    }

    public static function getNewCommentsCount()
    {
        return self::where('status', 0)->count();
    }
}
